import Shopify from 'shopify-api-node';
import {create as createNotication} from '../repositories/notificationRepository';
import {
  getImageSrcBylistProduct,
  getListProductIdByListOrder,
  getNotificationItem
} from '../helpers/shopifyHelper';

/**
 *
 * @param {*} param0
 */
export async function syncNotification({
  shopifyDomain,
  shopId,
  shopName,
  accessToken
}) {
  const shopify = new Shopify({
    shopName,
    accessToken
  });

  const listOrder = await shopify.order.list({
    limit: 30,
    fields: 'shipping_address, created_at, line_items'
  });

  const listNotification = await getNotificationItem(shopify, listOrder);

  await Promise.all(
    listNotification.map(notification =>
      createNotication({...notification, shopifyDomain, shopId})
    )
  );
}

/**
 *
 * @param {*} param0
 */
export async function registerWebhook({shopName, accessToken}) {
  const shopify = new Shopify({
    shopName,
    accessToken
  });

  await shopify.webhook.create({
    address: 'https://a36d-123-16-32-226.ap.ngrok.io/webhook/order/new',
    topic: 'orders/create'
  });
}
