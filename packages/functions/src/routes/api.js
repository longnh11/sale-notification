import Router from 'koa-router';
import {verifyRequest} from '@avada/shopify-auth';
import * as sampleController from '../controllers/sampleController';
import * as notificationController from '../controllers/notificationController';
import * as settingController from '../controllers/settingController';
import settingValidate from '../middleware/settingValidate';

const router = new Router({
  prefix: '/api'
});

router.use(verifyRequest());

router.get('/notifications', notificationController.listNotification);
router.get('/settings', settingController.getSetting);
router.put('/settings', settingValidate, settingController.putSetting);
router.get('/samples', sampleController.exampleAction);

export default router;
