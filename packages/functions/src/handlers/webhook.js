import App from 'koa';
import createErrorHandler from '../middleware/errorHandler';
import * as errorService from '../services/errorService';
import router from '../routes/webhook';
import cors from '@koa/cors';

// Initialize all demand configuration for an application
const api = new App();
api.proxy = true;
api.use(createErrorHandler());

// cors
api.use(cors());

// Register all routes for the application
api.use(router.allowedMethods());
api.use(router.routes());

// Handling all errors
api.on('error', errorService.handleError);

export default api;
