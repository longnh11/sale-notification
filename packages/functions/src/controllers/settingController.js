import * as settingRepo from '../repositories/settingRepository';
import {getCurrentUserInstance} from '../helpers/auth';

/**
 * 
 * @param {*} ctx 
 * @returns 
 */
export async function getSetting(ctx) {
  try {
    const {shopID} = getCurrentUserInstance(ctx);
  
    const setting = await settingRepo.getOne({shopID});
  
    return (ctx.body = {
      status: 200,
      success: true,
      data: setting
    });
  } catch (error) {
    return (ctx.body = {
      status: 400,
      success: false,
      error: error.message
    });
  }
}
/**
 * 
 * @param {*} ctx 
 * @returns 
 */
export async function putSetting(ctx){
  try {
    const {shopID} = getCurrentUserInstance(ctx);
    const putData = ctx.req.body

    await settingRepo.update(shopID, putData)

    return (ctx.body = {
      status: 200,
      success: true
    });
  } catch (error) {
    return (ctx.body = {
      status: 400,
      success: false,
      error: error.message
    });
  }
}
