import Shopify from 'shopify-api-node';
import {getOne as getShopByShopifyDomain} from '../repositories/shopRepository';
import {create as createNotification} from '../repositories/notificationRepository';
import {getNotificationItem} from '../helpers/shopifyHelper';

export async function newOrder(ctx) {
  try {
    const shopifyDomain = ctx.get('X-Shopify-Shop-Domain');
    const orderData = ctx.req.body;

    const shop = await getShopByShopifyDomain(shopifyDomain);
    const shopify = new Shopify({
      shopName: shop.name,
      accessToken: shop.accessToken
    });

    const [notification] = await getNotificationItem(shopify, [orderData]);
    await createNotification({...notification, shopId: shop.id, shopifyDomain});

    return (ctx.body = {
      success: true
    });
  } catch (err) {
    console.error(err);
    return (ctx.body = {
      success: false
    });
  }
}
