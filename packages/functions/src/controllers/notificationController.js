import * as notificationRepo from '../repositories/notificationRepository';
import {getCurrentUserInstance} from '../helpers/auth';

export async function listNotification(ctx) {
  try {
    const {id, handle, orderBy} = ctx.req.query;
    const {shopID} = getCurrentUserInstance(ctx);

    const queryString = {shopID, id, handle, orderBy};

    const notifications = await notificationRepo.paginate(queryString);

    return (ctx.body = {
      status: 200,
      success: true,
      data: notifications
    });
  } catch (error) {
    return (ctx.body = {
      status: 400,
      success: false,
      error: error.message
    });
  }
}
