import {paginate as listNotification} from '../repositories/notificationRepository';
import {getOne as getSetting} from '../repositories/settingRepository';
import {addFieldRelaviteDate} from '../helpers/notificationHelper'

export async function getListNotification(ctx) {
  try {
    const shopifyDomain = ctx.query.shopifyDomain;

    const [data, settings] = await Promise.all([
      listNotification({shopifyDomain}),
      getSetting({shopifyDomain})
    ]);

    ctx.status = 200;
    return (ctx.body = {
      data: addFieldRelaviteDate(data),
      settings
    });
  } catch (error) {
    ctx.status = 400;
    return (ctx.body = {
      error: error.message
    });
  }
}
