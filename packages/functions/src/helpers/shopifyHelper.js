/**
 *
 * @param {*} listOrder
 * @returns
 */
export function getListProductIdByListOrder(listOrder) {
  return listOrder
    .map(getProductIdInOrder)
    .filter(onlyUnique)
    .join(',');
}

/**
 *
 * @param {*} value
 * @param {*} index
 * @param {*} self
 * @returns
 */
export function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

/**
 *
 * @param {*} order
 * @returns
 */
export function getProductIdInOrder(order) {
  return order.line_items[0].product_id;
}

/**
 *
 * @param {*} listProductId
 * @returns
 */
export function getProductImageSrc(listProduct) {
  const result = {};

  listProduct.forEach(product => {
    result[product.image.product_id] = product.image.src;
  });
  return result;
}

/**
 *
 * @param {*} shopify
 * @param {*} orders
 * @returns
 */
export async function getNotificationItem(shopify, orders) {
  const productImage = await getProductImageByOrder(shopify, orders);

  return getListNotification(orders, productImage);
}

/**
 *
 * @param {*} shopify
 * @param {*} orders
 * @returns
 */
async function getProductImageByOrder(shopify, orders) {
  const listProductId = getListProductIdByListOrder(orders);
  const listProduct = await shopify.product.list({
    ids: listProductId,
    fields: 'image'
  });
  return getProductImageSrc(listProduct);
}

/**
 *
 * @param {*} orders
 * @param {*} productImage
 * @returns
 */
function getListNotification(orders, productImage) {
  return orders.map(order => {
    const product = order.line_items[0];

    const notification = {
      city: order.shipping_address.city,
      country: order.shipping_address.country,
      name: order.shipping_address.first_name,
      timestamp: new Date(order.created_at),
      productId: product.product_id,
      productName: product.name,
      productImage: productImage[product.product_id]
    };
    return notification;
  });
}
