import moment from 'moment';

export function addFieldRelaviteDate(data) {
  return data.map(item => ({
    ...item,
    ralativeDate: moment(item.timestamp).fromNow()
  }));
}
