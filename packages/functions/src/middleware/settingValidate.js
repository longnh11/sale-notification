import * as yup from 'yup';
/**
 * 
 * @param {*} ctx 
 * @param {*} next 
 */
export default async function settingValidate(ctx, next) {
  try {
    const data = ctx.req.body;

    const schema = yup.object().shape({
      position: yup
        .string()
        .oneOf(['bottom-left', 'bottom-right', 'top-left', 'top-right'])
        .required(),
      hideTimeAgo: yup.bool().required(),
      truncateProductName: yup.bool().required(),
      displayDuration: yup
        .number()
        .min(0)
        .max(80)
        .required(),
      firstDelay: yup
        .number()
        .min(0)
        .max(80)
        .required(),
      popsInterval: yup
        .number()
        .min(0)
        .max(80)
        .required(),
      maxPopsDisplay: yup
        .number()
        .min(0)
        .max(80)
        .required(),
      includedUrls: yup.string(),
      excludedUrls: yup.string(),
      allowShow: yup
        .string()
        .oneOf(['all', 'specific'])
        .required(),
      shopId: yup.string().required()
    });
    await schema.validate(data);
    await next();
  } catch (error) {
    console.log(error);
    ctx.status = 400;
    ctx.body = {
      success: false,
      errors: error.errors,
      errorName: error.name
    };
  }
}
