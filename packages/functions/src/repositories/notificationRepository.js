import {Firestore} from '@google-cloud/firestore';
import formatDateFields from '../helpers/formatDateFields';

const firestore = new Firestore();
const notificationRef = firestore.collection('notifications');

/**
 *
 * @param {*} page
 * @returns
 */
export async function paginate({
  shopifyDomain,
  shopID,
  id,
  handle = 'next',
  orderBy = 'timestamp_desc'
}) {
  const [field, direction] = orderBy.split('_');

  let docRef = notificationRef;

  if (shopID) {
    docRef = docRef.where('shopId', '==', shopID);
  }
  if (shopifyDomain) {
    docRef = docRef.where('shopifyDomain', '==', shopifyDomain);
  }

  docRef = docRef.orderBy(field, direction);

  if (handle === 'next') {
    if (id) {
      const doc = await notificationRef.doc(id).get();
      docRef = docRef.startAfter(doc);
    }
    docRef = docRef.limit(2);
  }
  if (handle === 'previous') {
    const doc = await notificationRef.doc(id).get();
    docRef = docRef.endBefore(doc).limitToLast(2);
  }

  const snapshot = await docRef.get();
  const datas = snapshot.docs.map(doc => ({
    id: doc.id,
    ...formatDateFields(doc.data())
  }));

  return datas;
  //page
}

/**
 *
 * @param {*} data
 */
export async function create(data) {
  const doc = await notificationRef.add(data);
  if (!doc) throw new Error('Add false');
}
