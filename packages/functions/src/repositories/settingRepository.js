import {Firestore} from '@google-cloud/firestore';

const firestore = new Firestore();
const settingRef = firestore.collection('settings');

/**
 * 
 * @param {*} param0 
 * @returns 
 */
export async function getOne({shopId, shopifyDomain}) {
  let docRef = settingRef

  if (shopId) {
    docRef = docRef.where('shopId', '==', shopId);
  }
  if (shopifyDomain) {
    docRef = docRef.where('shopifyDomain', '==', shopifyDomain);
  }

  const snapshot = await docRef.limit(1).get();
  const doc = snapshot.docs[0]
  
  return {id: doc.id, ...doc.data()};
}

export async function create(data){
  const doc = await settingRef.add(data)
  if (!doc) throw new Error('Create fail!')
}

/**
 * 
 * @param {*} shopId 
 * @param {*} data 
 */
export async function update(shopId, data){
  const snapshot = await settingRef.where('shopId', '==', shopId).limit(1).get()
  const id = snapshot.docs[0].id
  
  await settingRef.doc(id).update(data)
}
