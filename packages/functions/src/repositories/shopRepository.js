import {Firestore} from '@google-cloud/firestore';

const firestore = new Firestore();
const shopRef = firestore.collection('shops');

/**
 * 
 * @param {*} shopifyDomain 
 * @returns 
 */
export async function getOne(shopifyDomain) {
  const snapshot = await shopRef
    .where('shopifyDomain', '==', shopifyDomain)
    .limit(1)
    .get();

  const doc = snapshot.docs[0];

  return {id: doc.id, ...doc.data()};
}
