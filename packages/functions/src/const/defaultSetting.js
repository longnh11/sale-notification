 const defaultSetting = {
    position: 'bottom-left',
    hideTimeAgo: false,
    truncateProductName: false,
    displayDuration: 0,
    firstDelay: 0,
    popsInterval: 0,
    maxPopsDisplay: 0,
    includedUrls:'',
    excludedUrls:'',
    allowShow: 'all'
  };

  export default defaultSetting;