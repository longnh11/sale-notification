import React, {useState} from 'react';
import {
  Layout,
  Page,
  Tabs,
  Card,
  FormLayout,
  TextStyle,
  Checkbox,
  RangeSlider,
  TextField,
  Select,
  Button,
  SkeletonTabs
} from '@shopify/polaris';
import NotificationPopup from '../../components/NotificationPopup';
import DesktopPositionInput from '../../components/DesktopPositionInput';
import defaultSetting from '../../../../functions/src/const/defaultSetting';
import useFetchApi from '../../hooks/api/useFetchApi';
import {api} from '../../helpers';
import {store} from '../..';
import {setToast} from '../../actions/layout/setToastAction';

export default function Setting() {
  const [tabSelected, setTabSelected] = useState(0);

  const {data: input, setData: setInput, loading, setLoading} = useFetchApi(
    '/settings',
    defaultSetting
  );

  const handleChangeInput = (key, value) => {
    setInput(prev => ({
      ...prev,
      [key]: value
    }));
  };

  const handleSave = async () => {
    try {
      setLoading(true);
      await api('/settings', 'PUT', input);
      store.dispatch(
        setToast({
          content: 'Save successfully'
        })
      );
    } catch (error) {
      console.log(error);
      store.dispatch(
        setToast({
          content: 'Save fail'
        })
      );
    } finally {
      setLoading(false);
    }
  };

  const handleTabChange = value => setTabSelected(value);

  const options = [
    {label: 'All pages', value: 'all'},
    {label: 'Specific pages', value: 'specific'}
  ];

  const tabs = [
    {
      id: 'Display',
      content: 'Display',
      body: (
        <FormLayout>
          <Card.Section title="APPEARANCE" flush>
            <DesktopPositionInput
              label="Desktop Position"
              helpText="The display position of the pop on your website."
              onChange={val => handleChangeInput('position', val)}
              value={input.position}
            />
            <Checkbox
              label="Hide time ago"
              checked={input.hideTimeAgo}
              onChange={val => handleChangeInput('hideTimeAgo', val)}
            />
            <Checkbox
              label="Truncate product name"
              checked={input.truncateProductName}
              onChange={val => handleChangeInput('truncateProductName', val)}
              helpText="If your product name is long for one line, is will be truncated to 'Product Na...'"
            />
          </Card.Section>
          <Card.Section title="TIMING" flush>
            <FormLayout>
              <FormLayout.Group>
                <RangeSlider
                  min={0}
                  max={80}
                  label="Display duration"
                  value={input.displayDuration}
                  onChange={val => handleChangeInput('displayDuration', val)}
                  helpText="How long each pop will display on your page."
                  suffix={Suffix(input.displayDuration, 'displayDuration')}
                  output
                />
                <RangeSlider
                  min={0}
                  max={80}
                  label="Time before the first pop"
                  helpText="The delay time before the first notification."
                  value={input.firstDelay}
                  onChange={val => handleChangeInput('firstDelay', val)}
                  suffix={Suffix(input.firstDelay, 'firstDelay')}
                  output
                />
              </FormLayout.Group>
            </FormLayout>
            <FormLayout>
              <FormLayout.Group>
                <RangeSlider
                  min={0}
                  max={80}
                  label="Gap time between too pops"
                  helpText="The time interval between two popup notification."
                  value={input.popsInterval}
                  onChange={val => handleChangeInput('popsInterval', val)}
                  suffix={Suffix(input.popsInterval, 'popsInterval')}
                  output
                />
                <RangeSlider
                  min={0}
                  max={80}
                  label="Maximum of popups"
                  helpText="The maximum of number of popups are allowed to show after page loading. Maximum number is 80."
                  value={input.maxPopsDisplay}
                  onChange={val => handleChangeInput('maxPopsDisplay', val)}
                  suffix={Suffix(input.maxPopsDisplay, 'maxPopsDisplay')}
                  output
                />
              </FormLayout.Group>
            </FormLayout>
          </Card.Section>
        </FormLayout>
      )
    },
    {
      id: 'Trigger',
      content: 'Trigger',
      body: (
        <Card.Section title="PAGES RESTRICTION" flush>
          <FormLayout>
            <Select
              options={options}
              onChange={val => handleChangeInput('allowShow', val)}
              value={input.allowShow}
            />
            {input.allowShow === 'specific' && (
              <TextField
                label="Include pages"
                value={input.includedUrls}
                multiline={5}
                onChange={val => handleChangeInput('includedUrls', val)}
              />
            )}
            <TextField
              label="Exclude pages"
              value={input.excludedUrls}
              multiline={5}
              onChange={val => handleChangeInput('excludedUrls', val)}
            />
          </FormLayout>
        </Card.Section>
      )
    }
  ];

  const buttonSave = (
    <Button primary onClick={handleSave} loading={loading}>
      Save
    </Button>
  );

  return (
    <Page
      title="Settings"
      subtitle="Decide how your notification will display"
      primaryAction={buttonSave}
      fullWidth={true}
    >
      <Layout>
        <Layout.Section secondary>
          <NotificationPopup></NotificationPopup>
        </Layout.Section>
        <Layout.Section>
          <Card fullWidth>
            {loading ? (
              <SkeletonTabs />
            ) : (
              <Tabs
                tabs={tabs}
                selected={tabSelected}
                onSelect={val => handleTabChange(val)}
              >
                <Card.Section>{tabs[tabSelected].body}</Card.Section>
              </Tabs>
            )}
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );

  function Suffix(value, field) {
    return (
      <div style={{width: 150}}>
        <TextField
          value={value}
          type="number"
          onChange={val => handleChangeInput(field, val)}
          suffix="second(s)"
          max={80}
          min={0}
        />
      </div>
    );
  }
}
