import React, {useState} from 'react';
import {
  Page,
  ResourceList,
  ResourceItem,
  Stack,
  Pagination,
  Card,
  Layout
} from '@shopify/polaris';
import NotificationPopup from '../../components/NotificationPopup';
import {api} from '../../helpers';
import useFetchApi from '../../hooks/api/useFetchApi';
import {store} from '../..';
import {setToast} from '../../actions/layout/setToastAction';
import moment from 'moment';

export default function Notification() {
  const [orderBy, setOrderBy] = useState('timestamp_desc');
  const [selectedItems, setSelectedItems] = useState([]);

  const {data: items, setData: setItems, loading, setLoading} = useFetchApi(
    '/notifications'
  );

  const handlePrevious = () => {
    const params = {handle: 'previous', orderBy, id: items[0].id};
    fetchNotification(params);
  };

  const handleNext = () => {
    const params = {handle: 'next', orderBy, id: items[items.length - 1].id};
    fetchNotification(params);
  };

  const fetchNotification = async params => {
    try {
      setLoading(true);
      const res = await api('/notifications', 'GET', {}, params);
      if (res.data.length) {
        setItems(res.data);
        store.dispatch(
          setToast({
            content: 'Get notification successfully'
          })
        );
      } else {
        store.dispatch(
          setToast({
            content: 'Nothing change'
          })
        );
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const handleSort = value => {
    setOrderBy(value);
    const params = {handle: 'next', orderBy: value};
    fetchNotification(params);
  };

  const resourceName = {
    singular: 'notification',
    plural: 'notifications'
  };

  const promotedBulkActions = [
    {
      content: 'Edit notifications',
      onAction: () => console.log('Todo: implement bulk edit')
    }
  ];

  const bulkActions = [];

  return (
    <Page
      title="Notifications"
      subtitle="List of sales notification from Shopify"
      fullWidth={true}
    >
      <Layout>
        <Layout.Section>
          <Card>
            <ResourceList
              resourceName={resourceName}
              items={items}
              renderItem={renderItem}
              selectedItems={selectedItems}
              onSelectionChange={setSelectedItems}
              promotedBulkActions={promotedBulkActions}
              bulkActions={bulkActions}
              resolveItemId={resolveItemIds}
              sortValue={orderBy}
              sortOptions={[
                {label: 'Newest update', value: 'timestamp_desc'},
                {label: 'Oldest update', value: 'timestamp_asc'}
              ]}
              onSortChange={val => handleSort(val)}
              loading={loading}
            />
          </Card>
        </Layout.Section>
        <Layout.Section>
          <Stack distribution="center">
            <Pagination
              hasPrevious
              onPrevious={handlePrevious}
              hasNext
              onNext={handleNext}
            />
          </Stack>
        </Layout.Section>
      </Layout>
    </Page>
  );

  function renderItem(item, _, index) {
    const {id} = item;

    return (
      <ResourceItem id={id} sortOrder={index}>
        <Stack>
          <Stack.Item fill>
            <NotificationPopup {...item}></NotificationPopup>
          </Stack.Item>
          <Stack.Item>{moment(item.timestamp).format('[From] LL')}</Stack.Item>
        </Stack>
      </ResourceItem>
    );
  }

  function resolveItemIds({id}) {
    return id;
  }
}
