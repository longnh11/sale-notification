import React, {useState} from 'react';
import {Layout, Page, SettingToggle, TextStyle} from '@shopify/polaris';

/**
 * Render a home page for overview
 *
 * @return {React.ReactElement}
 * @constructor
 */
export default function Home() {
  const [active, setActive] = useState(false);

  const handleToggle = () => setActive(active => !active);

  const appStatus = active ? 'enabled' : 'disabled';
  const contentStatus = active ? 'Disable' : 'Enable';

  return (
    //layout
    <Page title="Home" fullWidth={true}>
      <Layout>
        <Layout.Section>
          <SettingToggle
            action={{
              content: contentStatus,
              onAction: handleToggle
            }}
            enabled={active}
          >
            App status is <TextStyle variation="strong">{appStatus}</TextStyle>.
          </SettingToggle>
        </Layout.Section>
      </Layout>
    </Page>
  );
}
