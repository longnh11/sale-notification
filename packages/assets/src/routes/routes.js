import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from '../loadables/Home';
import Setting from '../loadables/Setting';
import NotFound from '../loadables/NotFound';
import Notification from '../loadables/Notification';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/setting" component={Setting} />
    <Route exact path="/notification" component={Notification} />
    <Route path="*" component={NotFound} />
  </Switch>
);

export default Routes;
